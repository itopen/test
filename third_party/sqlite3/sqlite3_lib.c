#include "sqlite3_lib.h"

struct Sqlite3SyncOps sqlite3Sync[] = {
    {SQLITE3_SYNC_MODE_FULL, "PRAGMA synchronous = FULL"},
    {SQLITE3_SYNC_MODE_NORMAL, "PRAGMA synchronous = NORMAL"},
    {SQLITE3_SYNC_MODE_OFF, "PRAGMA synchronous = OFF"},
};

int CreateDatabase(char *dbname, sqlite3 **db)
{
    int ret = sqlite3_open(dbname, db);
    if (ret != SQLITE_OK) {
        printf("sqlite3_open error: %s\n", sqlite3_errmsg(*db));
        return -1;
    }
    return SQLITE_OK;
}

int SetDatabaseSync(sqlite3 *db, enum Sqlite3SyncMode mode)
{
    char *errmsg = NULL;
    if (db == NULL) {
        printf("db is NULL\n");
        return -1;
    }

    if (sqlite3_exec(db, sqlite3Sync[mode].sql, NULL, NULL, &errmsg)) {
        printf("sqlite3_exec error: %s\n", errmsg);
        sqlite3_free(errmsg);
        return -1;
    }

    return SQLITE_OK;
}

int CreateDataSheet(sqlite3 *db, const char *sql)
{
    char *errmsg = NULL;
    if (db == NULL) {
        printf("db is NULL\n");
        return -1;
    }

    if (sqlite3_exec(db, sql, NULL, NULL, &errmsg)) {
        printf("sqlite3_exec error: %s\n", errmsg);
        sqlite3_free(errmsg);
        return -1;
    }
    return SQLITE_OK;
}

int InsertDataValue(sqlite3 *db, const char *sheet, const char *column, const char *value)
{
    int i;
    char sql[1024];
    char *errmsg = NULL;
    if (db == NULL) {
        printf("db is NULL\n");
        return -1;
    }

    sprintf(sql, "insert into %s(%s) values (%s);", sheet, column, value);
    printf("sql: %s\n", sql);

    if (sqlite3_exec(db, sql, NULL, NULL, &errmsg)) {
        printf("sqlite3_exec error: %s\n", errmsg);
        sqlite3_free(errmsg);
        return -1;
    }

    return SQLITE_OK;
}

int CloseDatabase(sqlite3 *db)
{
    if (db == NULL) {
        return 0;
    }
    return sqlite3_close(db);
}

