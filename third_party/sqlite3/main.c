#include "sqlite3_lib.h"

#define CREATE_TABLE "create table if not exists dict(word text primary key, translation text);"

int main(int argc, char **argv)
{
    int i;
    int cnt = 1;
    int ret;
    FILE *fp = NULL;
    sqlite3 *db = NULL;
    char buf[1024] = {0};
    char value[1024] = {0};
    printf("start create database\n");
    // 1. 创建数据库
    ret = CreateDatabase("dictionary.db", &db);
    if (ret != SQLITE_OK) {
        printf("CreateDatabase error: %s\n", sqlite3_errmsg(db));
        return -1;
    }

    // 2. 设置数据库同步模式
    ret = SetDatabaseSync(db, SQLITE3_SYNC_MODE_FULL);
    if (ret != SQLITE_OK) {
        printf("SetDatabaseSync error: %s\n", sqlite3_errmsg(db));
        return -1;
    }

    // 3. 常见表格
    ret = CreateDataSheet(db, CREATE_TABLE);
    if (ret != SQLITE_OK) {
        printf("CreateDataSheet error: %s\n", sqlite3_errmsg(db));
        return -1;
    }

    // 4. 插入数据
    fp = fopen("dict.txt", "r");
    if (fp == NULL) {
        printf("open dict.txt error\n");
        return -1;
    }

    while (fgets(buf, sizeof(buf), fp) != NULL) {
        if (buf[strlen(buf)-1] == '\n') {
            buf[strlen(buf)-1] = '\0';
        }

        // 获取单词结尾地方并设置为\0, 以及翻译起始位置
        for (i = 0; buf[i] != ' '; i++);
        buf[i] = '\0';
        for(; buf[++i] == ' ';);

        // 插入数据到数据库
        snprintf(value, 1024, "\"%s\", \"%s\"", &buf[0], &buf[i]);
        printf("%d: value: %s\n", cnt++, value);
        ret = InsertDataValue(db, "dict", "word, translation", value);
        if (ret != SQLITE_OK) {
            printf("InsertDataValue error: %s\n", sqlite3_errmsg(db));
            return -1;
        }

        memset(buf, '\0', sizeof(buf));
        memset(value, '\0', sizeof(value));
    }

    fclose(fp);
    CloseDatabase(db);
    printf("close database success\n");
    return 0;
}
