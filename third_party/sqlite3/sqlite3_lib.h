#ifndef __SQLITE3_LIB_H__
#define __SQLITE3_LIB_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>

enum Sqlite3SyncMode {
    // 在每次写入操作完成之前，会等待数据被写入磁盘后再继续下一步操作。这种模式具有最高的数据完整性和可靠性，但性能会受到一定影响
    SQLITE3_SYNC_MODE_FULL = 0,
    // 在写入操作完成之后，会等待数据被写入磁盘的操作系统缓存中后再继续下一步操作。这种模式在数据完整性和性能之间达到了一种平衡，适用于大多数场景
    SQLITE3_SYNC_MODE_NORMAL,
    // 在写入操作完成后，不会等待数据被写入磁盘，直接继续下一步操作。这种模式具有最高的性能，但数据的完整性和可靠性可能会受到一定影响。适用于对数据完整性要求不高的场景，例如缓存数据
    SQLITE3_SYNC_MODE_OFF
};

struct Sqlite3SyncOps {
    int id;
    char *sql;
};

int CreateDatabase(char *dbname, sqlite3 **db);
int SetDatabaseSync(sqlite3 *db, enum Sqlite3SyncMode mode);
int CreateDataSheet(sqlite3 *db, const char *sql);
int InsertDataValue(sqlite3 *db, const char *sheet, const char *column, const char *value);
int CloseDatabase(sqlite3 *db);

#endif /* end of __SQLITE3_LIB_H__ */
