#include <gtest/gtest.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


class TestZBinder : public testing::Test{
public:
    static void SetUpTestSuite();
    static void TearDownTestSuite();
    virtual void SetUp()
    {
        std::cout << "-----------SetUP-----------" << std::endl;
    }
    virtual void TearDown()
    {
        std::cout << "-----------TearDown-----------" << std::endl;
    }
};

void TestZBinder::SetUpTestSuite()
{
    std::cout << "-----------TearDownTestSuite-----------" << std::endl;
}

void TestZBinder::TearDownTestSuite()
{
    std::cout << "-----------TearDownTestSuite-----------" << std::endl;
}


TEST(TestZBinder, OpenZBinder)
{
    int fd = open("/dev/zbinder", O_RDWR);
    EXPECT_GT(fd, 0);
    close(fd);
}

