#include <gtest/gtest.h>
#include "mathtest.h"

class TestMath : public testing::Test{
public:
    static void SetUpTestSuite();
    static void TearDownTestSuite();
    virtual void SetUp()
    {
        std::cout << "-----------SetUP-----------" << std::endl;
    }
    virtual void TearDown()
    {
        std::cout << "-----------TearDown-----------" << std::endl;
    }
};

void TestMath::SetUpTestSuite()
{
    std::cout << "-----------TearDownTestSuite-----------" << std::endl;
}

void TestMath::TearDownTestSuite()
{
    std::cout << "-----------TearDownTestSuite-----------" << std::endl;
}


TEST(TestMath, AddTest)
{
    int a = 3;
    int b = 4;
    int ret = add(a, b);
    EXPECT_EQ(a + b, ret);
}

TEST(TestMath, SubTest)
{
    int a = 3;
    int b = 4;
    int ret = sub(a, b);
    EXPECT_EQ(a - b, ret);
}

TEST(TestMath, MulTest)
{
    int a = 3;
    int b = 4;
    int ret = mul(a, b);
    EXPECT_EQ(a * b, ret);
}

TEST(TestMath, divisionTest)
{
    int a = 6;
    int b = 3;
    int ret = division(a, b);
    EXPECT_EQ(a / b, ret);
}

TEST(TestMath, divisionTestFailed)
{
    int a = 6;
    int b = 0;
    int ret = division(a, b);
    EXPECT_EQ(-1, ret);
}