#include "mathtest.h"
#include <stdio.h>

int add(int a, int b)
{
    return a + b;
}

int sub(int a, int b)
{
    return a - b;
}

int mul(int a, int b)
{
    return a * b;
}

int division(int a, int b)
{
    if (b == 0) {
        printf("The division is zero!\n");
        return -1;
    }
    return a / b;
}
